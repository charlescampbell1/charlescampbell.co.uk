const express = require ('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const db = require('./db')

// Require Routers
const personalDetailRouter = require('./routes/personal-detail-router')
const profileLinkRouter = require('./routes/profile-link-router')
const toolRouter = require('./routes/tool-router')
const projectRouter = require('./routes/project-router')
const highlightRouter = require('./routes/highlight-router')

const app = express()
const apiPort = 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.get('/', (req, res) => {
  res.send(`Server Port: ${apiPort}`)
})

app.use('/api', personalDetailRouter)
app.use('/api', profileLinkRouter)
app.use('/api', toolRouter)
app.use('/api', projectRouter)
app.use('/api', highlightRouter)

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))