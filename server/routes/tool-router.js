const express = require('express')
const ToolCtrl = require('../controllers/tool-ctrl')
const router = express.Router()

router.post('/tool', ToolCtrl.createTool)
router.put('/tool/:id', ToolCtrl.updateTool)
router.delete('/tool/:id', ToolCtrl.deleteTool)
router.get('/tool/:id', ToolCtrl.getToolById)
router.get('/tools', ToolCtrl.getTools)

module.exports = router