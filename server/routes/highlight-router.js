const express = require('express')
const HighlightCtrl = require('../controllers/highlight-ctrl')
const router = express.Router()

router.post('/highlight', HighlightCtrl.createHighlight)
router.put('/highlight/:id', HighlightCtrl.updateHighlight)
router.delete('/highlight/:id', HighlightCtrl.deleteHighlight)
router.get('/highlight/:id', HighlightCtrl.getHighlightById)
router.get('/highlights', HighlightCtrl.getHighlights)

module.exports = router