const express = require('express')
const ProfileLinkCtrl = require('../controllers/profile-link-ctrl')
const router = express.Router()

router.post('/profile_link', ProfileLinkCtrl.createProfileLink)
router.put('/profile_link/:id', ProfileLinkCtrl.updateProfileLink)
router.delete('/profile_link/:id', ProfileLinkCtrl.deleteProfileLink)
router.get('/profile_link/:id', ProfileLinkCtrl.getProfileLinkById)
router.get('/profile_links', ProfileLinkCtrl.getProfileLinks)

module.exports = router