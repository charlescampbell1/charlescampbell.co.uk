const express = require('express')
const PersonalDetailCtrl = require('../controllers/personal-detail-ctrl')
const router = express.Router()

router.post('/personal_detail', PersonalDetailCtrl.createPersonalDetail)
router.put('/personal_detail/:id', PersonalDetailCtrl.updatePersonalDetail)
router.delete('/personal_detail/:id', PersonalDetailCtrl.deletePersonalDetail)
router.get('/personal_detail/:id', PersonalDetailCtrl.getPersonalDetailById)
router.get('/personal_details', PersonalDetailCtrl.getPersonalDetails)
router.get('/personal_detail', PersonalDetailCtrl.getPersonalDetail)

module.exports = router