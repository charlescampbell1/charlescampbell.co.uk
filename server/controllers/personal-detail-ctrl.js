const PersonalDetail = require('../models/personal-detail-model')

createPersonalDetail = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide an input',
    })
  }

  const personal_detail = new PersonalDetail(body)

  if (!personal_detail) {
    return res.status(400).json({ success: false, error: err })
  }

  personal_detail
    .save()
    .then(() => {
      return res.status(201).json({
        success: true,
        id: personal_detail._id,
        message: 'Record created!',
      })
    })
    .catch(error => {
      return res.status(400).json({
      error,
      message: 'Record not created!',
    })
  })
}

getPersonalDetail = async (req, res) => {
  await PersonalDetail.findOne({}, {}, { sort: { 'created_at': -1 } }, (err, personal_detail) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!personal_detail) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: personal_detail })
  }).catch(err => console.log(err))
}


updatePersonalDetail = async (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide a body to update',
    })
  }

  PersonalDetail.findOne({ _id: req.params.id }, (err, personal_detail) => {
    if (err) {
      return res.status(404).json({
        err,
        message: 'Record not found!',
      })
    }

    personal_detail.name = body.name
    personal_detail.position = body.position
    personal_detail
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: personal_detail._id,
          message: 'Record updated!',
        })
      })
      .catch(error => {
        return res.status(404).json({
          error,
          message: 'Record not updated!',
        })
      })
  })
}

deletePersonalDetail = async (req, res) => {
  await PersonalDetail.findOneAndDelete({ _id: req.params.id }, (err, personal_detail) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!personal_detail) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
      }

      return res.status(200).json({ success: true, data: personal_detail })
    }).catch(err => console.log(err))
}

getPersonalDetailById = async (req, res) => {
  await PersonalDetail.findOne({ _id: req.params.id }, (err, personal_detail) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!personal_detail) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: personal_detail })
  }).catch(err => console.log(err))
}

getPersonalDetails = async (req, res) => {
  await PersonalDetail.find({}, (err, movies) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!movies.length) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: movies })
  }).catch(err => console.log(err))
}

module.exports = {
  createPersonalDetail,
  updatePersonalDetail,
  deletePersonalDetail,
  getPersonalDetails,
  getPersonalDetailById,
  getPersonalDetail
}