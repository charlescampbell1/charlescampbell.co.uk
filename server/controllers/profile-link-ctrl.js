const ProfileLink = require('../models/profile-link-model')

createProfileLink = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide an input',
    })
  }

  const profile_link = new ProfileLink(body)

  if (!profile_link) {
    return res.status(400).json({ success: false, error: err })
  }

  profile_link
    .save()
    .then(() => {
      return res.status(201).json({
        success: true,
        id: profile_link._id,
        message: 'Record created!',
      })
    })
    .catch(error => {
      return res.status(400).json({
      error,
      message: 'Record not created!',
    })
  })
}

getProfileLink = async (req, res) => {
  await ProfileLink.findOne({}, {}, { sort: { 'created_at': -1 } }, (err, profile_link) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!profile_link) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: profile_link })
  }).catch(err => console.log(err))
}

updateProfileLink = async (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide a body to update',
    })
  }

  ProfileLink.findOne({ _id: req.params.id }, (err, profile_link) => {
    if (err) {
      return res.status(404).json({
        err,
        message: 'Record not found!',
      })
    }

    profile_link.name = body.name
    profile_link.icon = body.icon
    profile_link.url = body.url
    profile_link.position = body.position
    profile_link
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: profile_link._id,
          message: 'Record updated!',
        })
      })
      .catch(error => {
        return res.status(404).json({
          error,
          message: 'Record not updated!',
        })
      })
  })
}

deleteProfileLink = async (req, res) => {
  await ProfileLink.findOneAndDelete({ _id: req.params.id }, (err, profile_link) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!profile_link) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
      }

      return res.status(200).json({ success: true, data: profile_link })
    }).catch(err => console.log(err))
}

getProfileLinkById = async (req, res) => {
  await ProfileLink.findOne({ _id: req.params.id }, (err, profile_link) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!profile_link) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: profile_link })
  }).catch(err => console.log(err))
}

getProfileLinks = async (req, res) => {
  await ProfileLink.find({}, (err, movies) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!movies.length) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: movies })
  }).catch(err => console.log(err))
}

module.exports = {
  createProfileLink,
  updateProfileLink,
  deleteProfileLink,
  getProfileLinks,
  getProfileLinkById,
  getProfileLink
}