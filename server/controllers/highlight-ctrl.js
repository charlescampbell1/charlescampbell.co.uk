const Highlight = require('../models/highlight-model')

createHighlight = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide an input',
    })
  }

  const highlight = new Highlight(body)

  if (!highlight) {
    return res.status(400).json({ success: false, error: err })
  }

  highlight
    .save()
    .then(() => {
      return res.status(201).json({
        success: true,
        id: highlight._id,
        message: 'Record created!',
      })
    })
    .catch(error => {
      return res.status(400).json({
      error,
      message: 'Record not created!',
    })
  })
}

getHighlight = async (req, res) => {
  await Highlight.findOne({}, {}, { sort: { 'created_at': -1 } }, (err, highlight) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!highlight) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: highlight })
  }).catch(err => console.log(err))
}


updateHighlight = async (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide a body to update',
    })
  }

  Highlight.findOne({ _id: req.params.id }, (err, highlight) => {
    if (err) {
      return res.status(404).json({
        err,
        message: 'Record not found!',
      })
    }

    highlight.title = body.title
    highlight.company = body.company
    highlight.date_from = body.date_from
    highlight.date_to = body.date_to
    highlight.description = body.description
    highlight
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: highlight._id,
          message: 'Record updated!',
        })
      })
      .catch(error => {
        return res.status(404).json({
          error,
          message: 'Record not updated!',
        })
      })
  })
}

deleteHighlight = async (req, res) => {
  await Highlight.findOneAndDelete({ _id: req.params.id }, (err, highlight) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!highlight) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
      }

      return res.status(200).json({ success: true, data: highlight })
    }).catch(err => console.log(err))
}

getHighlightById = async (req, res) => {
  await Highlight.findOne({ _id: req.params.id }, (err, highlight) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!highlight) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: highlight })
  }).catch(err => console.log(err))
}

getHighlights = async (req, res) => {
  await Highlight.find({}, (err, movies) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!movies.length) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: movies })
  }).catch(err => console.log(err))
}

module.exports = {
  createHighlight,
  updateHighlight,
  deleteHighlight,
  getHighlights,
  getHighlightById,
  getHighlight
}