const Tool = require('../models/tool-model')

createTool = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide an input',
    })
  }

  const tool = new Tool(body)

  if (!tool) {
    return res.status(400).json({ success: false, error: err })
  }

  tool
    .save()
    .then(() => {
      return res.status(201).json({
        success: true,
        id: tool._id,
        message: 'Record created!',
      })
    })
    .catch(error => {
      return res.status(400).json({
      error,
      message: 'Record not created!',
    })
  })
}

getTool = async (req, res) => {
  await Tool.findOne({}, {}, { sort: { 'created_at': -1 } }, (err, tool) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!tool) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: tool })
  }).catch(err => console.log(err))
}


updateTool = async (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide a body to update',
    })
  }

  Tool.findOne({ _id: req.params.id }, (err, tool) => {
    if (err) {
      return res.status(404).json({
        err,
        message: 'Record not found!',
      })
    }

    tool.tool = body.tool
    tool.color = body.color
    tool
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: tool._id,
          message: 'Record updated!',
        })
      })
      .catch(error => {
        return res.status(404).json({
          error,
          message: 'Record not updated!',
        })
      })
  })
}

deleteTool = async (req, res) => {
  await Tool.findOneAndDelete({ _id: req.params.id }, (err, tool) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!tool) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
      }

      return res.status(200).json({ success: true, data: tool })
    }).catch(err => console.log(err))
}

getToolById = async (req, res) => {
  await Tool.findOne({ _id: req.params.id }, (err, tool) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!tool) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: tool })
  }).catch(err => console.log(err))
}

getTools = async (req, res) => {
  await Tool.find({}, (err, movies) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!movies.length) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: movies })
  }).catch(err => console.log(err))
}

module.exports = {
  createTool,
  updateTool,
  deleteTool,
  getTools,
  getToolById,
  getTool
}