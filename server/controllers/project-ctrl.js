const Project = require('../models/project-model')

createProject = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide an input',
    })
  }

  const project = new Project(body)

  if (!project) {
    return res.status(400).json({ success: false, error: err })
  }

  project
    .save()
    .then(() => {
      return res.status(201).json({
        success: true,
        id: project._id,
        message: 'Record created!',
      })
    })
    .catch(error => {
      return res.status(400).json({
      error,
      message: 'Record not created!',
    })
  })
}

getProject = async (req, res) => {
  await Project.findOne({}, {}, { sort: { 'created_at': -1 } }, (err, project) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!project) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: project })
  }).catch(err => console.log(err))
}


updateProject = async (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide a body to update',
    })
  }

  Project.findOne({ _id: req.params.id }, (err, project) => {
    if (err) {
      return res.status(404).json({
        err,
        message: 'Record not found!',
      })
    }

    project.name = body.name
    project.url = body.url
    project
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: project._id,
          message: 'Record updated!',
        })
      })
      .catch(error => {
        return res.status(404).json({
          error,
          message: 'Record not updated!',
        })
      })
  })
}

deleteProject = async (req, res) => {
  await Project.findOneAndDelete({ _id: req.params.id }, (err, project) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!project) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
      }

      return res.status(200).json({ success: true, data: project })
    }).catch(err => console.log(err))
}

getProjectById = async (req, res) => {
  await Project.findOne({ _id: req.params.id }, (err, project) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!project) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: project })
  }).catch(err => console.log(err))
}

getProjects = async (req, res) => {
  await Project.find({}, (err, movies) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!movies.length) {
      return res
        .status(404)
        .json({ success: false, error: `Record not found` })
    }

    return res.status(200).json({ success: true, data: movies })
  }).catch(err => console.log(err))
}

module.exports = {
  createProject,
  updateProject,
  deleteProject,
  getProjects,
  getProjectById,
  getProject
}