const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Tool = new Schema(
  {
    tool: { type: String, required: true },
    color: { type: String, required: true }
  }
)

module.exports = mongoose.model('tool', Tool)