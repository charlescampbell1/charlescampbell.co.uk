const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Highlight = new Schema(
  {
    title: { type: String, required: true },
    company: { type: String, required: true },
    date_from: { type: Date, required: true },
    date_to: { type: Date, required: false },
    description: { type: String, required: true }
  }
)

module.exports = mongoose.model('highlight', Highlight)