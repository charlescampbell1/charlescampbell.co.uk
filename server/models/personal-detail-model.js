const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PersonalDetail = new Schema(
  {
    name: { type: String, required: true },
    position: { type: String, required: true }
  }
)

module.exports = mongoose.model('personal detail', PersonalDetail)