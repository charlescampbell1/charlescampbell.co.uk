const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProfileLink = new Schema(
  {
    name: { type: String, required: true },
    icon: { type: String, required: true },
    url: { type: String, required: true },
    position: { type: Number }
  }
)

module.exports = mongoose.model('profile link', ProfileLink)