import React, { Component } from 'react'
import './styles.scss'

class ProjectLink extends Component {
  render() {
    return (
      <a className='project-link' href={this.props.link}>{this.props.display}</a>
      )
  }
}

export default ProjectLink