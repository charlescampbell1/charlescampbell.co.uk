import React, { Component } from 'react'
import './styles.scss'

class Heading extends Component {
  render() {
    return (
      <div className='col'>
        <p className='page-heading'>{this.props.title}</p>
        <hr className='project-ruler' />
      </div>
    )
  }
}

export default Heading