import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './styles.scss'

class SocialLink extends Component {
  render() {
    return(
      <li>
        <a className='social-link' href={this.props.link}>
          <FontAwesomeIcon icon={this.props.icon} />
        </a>
      </li>
    )
  }
}

export default SocialLink