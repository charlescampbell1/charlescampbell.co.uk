import React, { Component } from 'react'

import './styles.scss'

class Tool extends Component {
  render() {
    return(
      <span className={'tool '+this.props.color} >{this.props.tool}</span>
    )
  }
}

export default Tool