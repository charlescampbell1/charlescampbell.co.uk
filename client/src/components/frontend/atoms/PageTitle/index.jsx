import React, { Component } from 'react'
import './styles.scss'

class PageTitle extends Component {
  render() {
    return (
      <div className="col-lg-4">
        <div className="heading">
          <h3>{this.props.title}</h3>
          <h6>{this.props.subtitle}</h6>
        </div>
      </div>
    )
  }
}

export default PageTitle