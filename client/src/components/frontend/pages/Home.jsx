import React, { Component } from 'react'
import { Experience, Toolkit } from '../../frontend/organisms'

class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Experience />
        <Toolkit />
      </React.Fragment>
    )
  }
}

export default Home;