import React, { Component } from 'react'
import { PageTitle } from '../atoms'
import { AcademicYear } from '../../frontend/organisms'

class Education extends Component {
  render() {
    return (
      <div className="row">
      <PageTitle title='BSc Software Engineering' subtitle='Bournemouth University'/>

        <div className='col-lg-8'>
          <AcademicYear title='Final Year - Software Engineering' grade='AUG 2021'/>
          <AcademicYear title='Second Year - Software Engineering' grade='FIRST'/>
        </div>
      </div>
    )
  }
}

export default Education