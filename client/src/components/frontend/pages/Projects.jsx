import React, { Component } from 'react'
import { Heading, ProjectLink } from '../atoms'
import api from '../../../api'

class Projects extends Component {
  constructor(props) {
      super(props)
      this.state = {
        projects: []
      }
  }

  componentDidMount = async () => {
    await api.getAllProjects().then(projects => {
      this.setState({
        projects: projects.data.data
      })
    })
  }

  render() {
    const projects = []

    this.state.projects.forEach((project) => {
      projects.push(
        <ProjectLink display={project.name} link={project.url} />
      )
    })

    return (
      <div>
        <Heading title='Current Projects'/>
        <div className="project-links">
          { projects }
        </div>
      </div>
    )
  }
}

export default Projects