import Home from './Home'
import Education from './Education'
import Projects from './Projects'

export { Home, Education, Projects }