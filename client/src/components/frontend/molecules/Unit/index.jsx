import React, { Component } from 'react'
import { Tool } from '../../atoms'

import './styles.scss'

class Unit extends Component {
  render() {
    return(
      <React.Fragment>
        <div className='unit'>
          <h5>{this.props.title} <small className='primary-color'>{this.props.grade}</small></h5>
          <p className="margin-tb-10">The second half of the semester will leave me to focus on my final year project &amp; dissertation. I will be developing a business management application specialising in finance called QuayBooks. You can find the early stages of this project in my 'projects' section.  </p>

          <div className='tools'>
            <Tool tool='ruby' color='red'/>
            <Tool tool='rails' color='red'/>
            <Tool tool='git'/>
          </div>
          <hr className='education-ruler'/>
        </div>
      </React.Fragment>
    )
  }
}

export default Unit