import { faGithub, faGitlab, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
import React, { Component } from 'react'
import { SocialLink } from '../../atoms'
import { faStickyNote, faEnvelope } from '@fortawesome/free-regular-svg-icons'

import './styles.scss'

class SocialLinks extends Component {
  render() {
    return(
      <ul>
        <SocialLink link='#' icon={faEnvelope}/>
        <SocialLink link='#' icon={faLinkedinIn}/>
        <SocialLink link='#' icon={faGitlab}/>
        <SocialLink link='#' icon={faGithub}/>
        <SocialLink link='#' icon={faStickyNote}/>
      </ul>
    )
  }
}

export default SocialLinks