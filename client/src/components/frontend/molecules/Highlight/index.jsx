import React, { Component } from 'react'
import Moment from 'moment'

class Highlight extends Component {
  render() {
    const title = this.props.title
    const company = this.props.company
    const date_from = Moment(this.props.date_from).format('MMM yyyy')
    const date_to = Moment(this.props.date_to).format('MMM yyyy')
    const description = this.props.description


    return(
      <div className='highlight'>
        <h4>{title}</h4>
        <h5>{company}</h5>
        <h6 className="font-light-black margin-t-10">{date_from} - {date_to}</h6>
        <p className="margin-tb-10">{description}</p>
      </div>
    )
  }
}

export default Highlight
