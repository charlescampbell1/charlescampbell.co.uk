import React, { Component } from 'react'
import { PageTitle } from '../../atoms'
import api from '../../../../api'
import './styles.scss'
import { Highlight } from '../../molecules'

class Experience extends Component {
  constructor(props) {
  super(props)
    this.state = {
      highlights: []
    }
  }

  componentDidMount = async () => {
    await api.getAllHighlights().then(highlights => {
      this.setState({
        highlights: highlights.data.data
      })
    })
  }

  render() {
    const highlights = []

    this.state.highlights.forEach((highlight) => {
      highlights.push(
        <Highlight
          title={highlight.title}
          company={highlight.company}
          date_from={highlight.date_from}
          date_to={highlight.date_to}
          description={highlight.description}
        />
        )
    })

    return (
      <div className="row">
        <PageTitle title='Experience' subtitle='highlights'/>

        <div className='col-lg-8'>
          {highlights}
        </div>
      </div>
    )
  }
}

export default Experience
