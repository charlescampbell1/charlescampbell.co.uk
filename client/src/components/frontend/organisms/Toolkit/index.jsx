import React, { Component } from 'react';
import { PageTitle, Tool } from '../../atoms';
import api from '../../../../api'

import './styles.scss'

class Toolkit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tools: []
    }
  }

  componentDidMount = async () => {
    await api.getAllTools().then(tools => {
      this.setState({
        tools: tools.data.data
      })
    })
  }

  render() {
    const tools = []

    this.state.tools.forEach((tool) => {
      tools.push(
        <Tool tool={tool.tool} color={tool.color}/>
      )
    })

    return (
      <div className='row'>
        <PageTitle title='Toolkit' subtitle='tools & technologies' />

        <div className="col-lg-8">
          <div className='tools'>
            { tools }
          </div>
        </div>
      </div>
    )
  }
}

export default Toolkit