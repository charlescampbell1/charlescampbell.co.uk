import React, { Component } from 'react'
import { Unit } from '../../molecules'
import './styles.scss'

class AcademicYear extends Component {
  render() {
    return(
      <React.Fragment>
        <h4>{this.props.title} <small className='primary-color'>{this.props.grade}</small></h4>
        <Unit title='Project Dissertation' grade='AUG 2021'/>
      </React.Fragment>
    )
  }
}

export default AcademicYear