import React, { Component } from 'react'
import { SocialLinks } from '../../molecules'
import './styles.scss'
import api from '../../../../api'

class Intro extends Component {
  constructor(props) {
    super(props)
    this.state = {
      personalDetail: []
    }
  }

  componentDidMount = async () => {
    await api.getPersonalDetail().then(personalDetail => {
      this.setState({
        personalDetail: personalDetail.data.data,
      })
    })
  }

  render() {
    return(
      <div className='intro'>
        <h2 className='name'>{this.state.personalDetail.name}</h2>
        <h4 className='position'>{this.state.personalDetail.position}</h4>
        <SocialLinks />
      </div>
    )
  }
}

export default Intro