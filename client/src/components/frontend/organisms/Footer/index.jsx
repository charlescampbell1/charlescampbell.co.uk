import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './styles.scss'

class Footer extends Component {
  render() {
    return(
      <footer>
        <NavLink activeClassName='active' className='nav-link' to='/projects'>Projects</NavLink>
        <small>/</small>

        <NavLink exact activeClassName='active' className='nav-link' to='/'>Home</NavLink>
        <small>/</small>

        <NavLink activeClassName='active' className='nav-link' to='/education'>Education</NavLink>
        <small>/</small>

        <NavLink activeClassName='active' className='nav-link' to='/backend'>Backend</NavLink>
      </footer>
    )
  }
}

export default Footer