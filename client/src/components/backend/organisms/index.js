import PersonalDetail from './PersonalDetail'
import ProfileLinks from './ProfileLinks'
import Tools from './Tools'
import Projects from './Projects'
import Highlights from './Highlights'

export { PersonalDetail, ProfileLinks, Tools, Projects, Highlights }
