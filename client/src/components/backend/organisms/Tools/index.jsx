import React, { Component } from 'react'
import MUIDataTable from 'mui-datatables'
import { Tool } from '../../../frontend/atoms'
import api from '../../../../api'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons'

class UpdateTool extends Component {
  updateTool = event => {
    event.preventDefault()
    window.location.href = `/tool/update/${this.props.id}`
  }

  render() {
    return(
      <a className='icon-link' onClick={this.updateTool}>
        <FontAwesomeIcon icon={faEdit} />
      </a>
    )
  }
}

class DeleteTool extends Component {
  deleteTool = event => {
    event.preventDefault()
    if(window.confirm(`Do you want to delete this tool?`)) {
      api.deleteToolById(this.props.id)
      window.location.reload()
    }
  }

  render() {
    return(
      <a className='icon-link danger' onClick={this.deleteTool}>
        <FontAwesomeIcon icon={faTrashAlt} />
      </a>
    )
  }
}

class Tools extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tools: [],
      columns: []
    }
  }

  componentDidMount = async () => {
    await api.getAllTools().then(tools=> {
      this.setState({
        tools: tools.data.data,
        isLoading: false
      })
    })
  }

  render() {
    const dataRows = []

    this.state.tools.forEach((tool) => {
      dataRows.push(
        [
          <Tool tool={tool.tool} color={tool.color} />,
          <UpdateTool id={tool._id} />,
          <DeleteTool id={tool._id} />
        ]
      )
    })

    const options = {
      filter: true,
      filterType: 'dropdown',
      responsive: 'standard'
    }

    const columns = [
      { name: 'Tool', options: { filter: false } },
      { name: '', options: { filter: false } },
      { name: '', options: { filter: false } }
    ]

    return (
      <MUIDataTable data={dataRows} columns={columns} options={options} />
    )
  }
}

export default Tools