import React, { Component } from 'react'
import MUIDataTable from 'mui-datatables'
import api from '../../../../api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons'

class UpdateProject extends Component {
  updateProject = event => {
    event.preventDefault()
    window.location.href = `/project/update/${this.props.id}`
  }

  render() {
    return(
      <a className='icon-link' onClick={this.updateProject}>
        <FontAwesomeIcon icon={faEdit} />
      </a>
    )
  }
}

class DeleteProject extends Component {
  deleteProject = event => {
    event.preventDefault()
    if(window.confirm(`Do you want to delete this project?`)) {
      api.deleteProjectById(this.props.id)
      window.location.reload()
    }
  }

  render() {
    return(
      <a className='icon-link danger' onClick={this.deleteProject}>
        <FontAwesomeIcon icon={faTrashAlt} />
      </a>
    )
  }
}


class Projects extends Component {
  constructor(props) {
    super(props)
    this.state = {
      projects: [],
      columns: []
    }
  }

  componentDidMount = async () => {
    await api.getAllProjects().then(projects=> {
      this.setState({
        projects: projects.data.data,
        isLoading: false
      })
    })
  }

  render() {
    const dataRows = []

    this.state.projects.forEach((project, i) => {
      dataRows.push(
        [
          project.name,
          project.url,
          <UpdateProject id={project._id} />,
          <DeleteProject id={project._id} />
        ]
      )
    })

    const options = {
      filter: true,
      filterType: 'dropdown',
      responsive: 'standard'
    }

    const columns = [
      { name: 'Name', options: { filter: false } },
      { name: 'Url', options: { filter: false } },
      { name: '', options: { filter: false } },
      { name: '', options: { filter: false } }
    ]

    return (
      <MUIDataTable data={dataRows} columns={columns} options={options} />
    )
  }
}

export default Projects