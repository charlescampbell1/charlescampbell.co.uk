import React, { Component } from 'react'
import MUIDataTable from 'mui-datatables'
import api from '../../../../api'


class ProfileLinks extends Component {
  constructor(props) {
    super(props)
    this.state = {
      profileLinks: [],
      columns: []
    }
  }

  componentDidMount = async () => {
    await api.getAllProfileLinks().then(profileLinks=> {
      this.setState({
        profileLinks: profileLinks.data.data,
        isLoading: false
      })
    })
  }

  render() {
    const dataRows = []

    this.state.profileLinks.forEach((item, i) => {
      dataRows.push(
        [
          item.name,
          item.icon,
          item.url,
          item.position
        ]
      )
    })

    const options = {
      filter: true,
      filterType: 'dropdown',
      responsive: 'standard'
    }

    const columns = [
      { name: 'Name', options: { filter: false } },
      { name: 'Icon', options: { filter: false } },
      { name: 'Url', options: { filter: false } },
      { name: 'Position', options: { filter: false } }
    ]

    return (
      <MUIDataTable data={dataRows} columns={columns} options={options} />
    )
  }
}

export default ProfileLinks