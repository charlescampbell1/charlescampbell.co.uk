import React, { Component } from 'react'
import MUIDataTable from 'mui-datatables'
import api from '../../../../api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons'

class UpdateHighlight extends Component {
  updateHighlight = event => {
    event.preventDefault()
    window.location.href = `/highlight/update/${this.props.id}`
  }

  render() {
    return(
      <a className='icon-link' onClick={this.updateHighlight}>
        <FontAwesomeIcon icon={faEdit} />
      </a>
    )
  }
}

class DeleteHighlight extends Component {
  deleteHighlight = event => {
    event.preventDefault()
    if(window.confirm(`Do you want to delete this highlight?`)) {
      api.deleteHighlightById(this.props.id)
      window.location.reload()
    }
  }

  render() {
    return(
      <a className='icon-link danger' onClick={this.deleteHighlight}>
        <FontAwesomeIcon icon={faTrashAlt} />
      </a>
    )
  }
}

class Highlight extends Component {
  constructor(props) {
    super(props)
    this.state = {
      highlights: [],
      columns: []
    }
  }

  componentDidMount = async () => {
    await api.getAllHighlights().then(highlights=> {
      this.setState({
        highlights: highlights.data.data,
        isLoading: false
      })
    })
  }

  render() {
    const dataRows = []

    this.state.highlights.forEach((item, i) => {
      dataRows.push(
        [
          item.title,
          item.company,
          item.date_from,
          item.date_to,
          item.description,
          <UpdateHighlight id={item._id} />,
          <DeleteHighlight id={item._id} />
        ]
      )
    })

    const options = {
      filter: true,
      filterType: 'dropdown',
      responsive: 'standard'
    }

    const columns = [
      { name: 'Title', options: { filter: false } },
      { name: 'Company', options: { filter: false } },
      { name: 'Date from:', options: { filter: false } },
      { name: 'Date to:', options: { filter: false } },
      { name: 'Description:', options: { filter: false } },
      { name: '', options: { filter: false } },
      { name: '', options: { filter: false } }
    ]

    return (
      <MUIDataTable data={dataRows} columns={columns} options={options} />
    )
  }
}

export default Highlight
