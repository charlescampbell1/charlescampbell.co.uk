import React, { Component } from 'react'
import MUIDataTable from 'mui-datatables'
import api from '../../../../api'
import './styles.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons'
class UpdatePersonalDetail extends Component {
  updatePersonalDetail = event => {
    event.preventDefault()
    window.location.href = `/personal_detail/update/${this.props.id}`
  }

  render() {
    return(
      <a className='icon-link' onClick={this.updatePersonalDetail}>
        <FontAwesomeIcon icon={faEdit} />
      </a>
    )
  }
}

class DeletePersonalDetail extends Component {
  deletePersonalDetail = event => {
    event.preventDefault()
    if(window.confirm(`Do you want to delete this personal detail?`)) {
      api.deletePersonalDetailById(this.props.id)
      window.location.reload()
    }
  }

  render() {
    return(
      <a className='icon-link danger' onClick={this.deletePersonalDetail}>
        <FontAwesomeIcon icon={faTrashAlt} />
      </a>
    )
  }
}

class PersonalDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      personalDetails: [],
      columns: []
    }
  }

  componentDidMount = async () => {
    await api.getAllPersonalDetails().then(personalDetails=> {
      this.setState({
        personalDetails: personalDetails.data.data,
        isLoading: false
      })
    })
  }

  render() {
    const dataRows = []

    this.state.personalDetails.forEach((item, i) => {
      dataRows.push(
        [
          item.name,
          item.position,
          <UpdatePersonalDetail id={item._id} />,
          <DeletePersonalDetail id={item._id} />
        ]
      )
    })

    const options = {
      filter: true,
      filterType: 'dropdown',
      responsive: 'standard'
    }

    const columns = [
      { name: 'Name', options: { filter: false } },
      { name: 'Position', options: { filter: false } },
      { name: '', options: { filter: false } },
      { name: '', options: { filter: false } }
    ]

    return (
      <MUIDataTable data={dataRows} columns={columns} options={options} />
    )
  }
}

export default PersonalDetail