import BackendHome from './BackendHome'

import NewPersonalDetail from './PersonalDetails/create'
import UpdatePersonalDetail from './PersonalDetails/update'

import CreateTool from './Tools/create'
import UpdateTool from './Tools/update'

import CreateProject from './Projects/create'
import UpdateProject from './Projects/update'

import CreateHighlight from './Highlights/create'
import UpdateHighlight from './Highlights/update'

export {
  BackendHome,
  NewPersonalDetail, UpdatePersonalDetail,
  CreateTool, UpdateTool,
  CreateProject, UpdateProject,
  CreateHighlight, UpdateHighlight
 }