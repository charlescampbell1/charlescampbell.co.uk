import React, { Component } from 'react'
import { PersonalDetail, ProfileLinks, Tools, Projects, Highlights } from '../organisms'
import { Heading } from '../../frontend/atoms'
import { Link } from '@material-ui/core'

class BackendHome extends Component {
  render() {
    return (
      <React.Fragment>
        <Heading title='New Information' />

        <Link href='/personal_detail/new'>New Intro</Link>
        <Link href='/profile_link/new'>New Link</Link>
        <Link href='/tool/new'>New Tool</Link>
        <Link href='/project/new'>New Project</Link>
        <Link href='/highlight/new'>New Highlight</Link>
        <hr />

        <Heading title='highlights' />
        <Highlights />

        <Heading title='Personal Data' />
        <PersonalDetail />
        <hr />

        <Heading title='Toolkit' />
        <Tools />
        <hr />

        <Heading title='Social Links' />
        <ProfileLinks />

        <Heading title='Projects' />
        <Projects />

      </React.Fragment>
    )
  }
}

export default BackendHome