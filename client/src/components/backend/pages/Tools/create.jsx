import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { TextField } from 'mui-rff'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'

const formFields = [
  { size: 12, field: (<TextField label='Tool' name='tool' margin='none' required={true} />) },
  { size: 12, field: (<TextField label='Colour' name='color' margin='none' required={true} />) }
]

const validate = values => {
  const errors = {};

  if(!values.tool) { errors.tool = 'Required!' }
}

const onSubmit = async values => {
  await api.insertTool(values).then(res => {
    window.alert('Tool created.')
  })
}

class ToolCreate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tool: '',
      color: ''
    }
  }

  render () {
    return(
      <React.Fragment>
        <Heading title='Create Tool'/>

        <Form onSubmit={onSubmit} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button type='button' variant='contained' onClick={form.reset} disabled={submitting || pristine}>Reset</Button>
                </Grid>

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default ToolCreate