import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { TextField } from 'mui-rff'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'

const validate = values => {
  const errors = {};

  if(!values.tool) { errors.tool = 'Required!' }
  if(!values.color) { errors.color = 'Required!' }
}

class ToolUpdate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      tool: '',
      color: ''
    }
  }

  componentDidMount = async () => {
    const { id } = this.state
    const tool = await api.getToolById(id)

    this.setState({
      tool: tool.data.data.tool,
      color: tool.data.data.color
    })
  }

  handleChangeInputTool = async event => {
    const tool = event.target.value
    this.setState({ tool })
  }

  handleChangeInputColor = async event => {
    const color = event.target.value
    this.setState({ color })
  }

  handleUpdateTool = async () => {
    const { id, tool, color } = this.state
    const payload = { tool, color }

    await api.updateToolById(id, payload).then(res => {
      window.alert(`Tool updated.`)
      this.setState({
        tool: '',
        color: ''
      })
      this.props.history.push('/backend')
      window.location.reload()
    })
  }

  render() {
    const { tool, color } = this.state

    const formFields = [
      { size: 12, field: (<TextField label='Tool' name='tool' value={tool} margin='none' required={true} onChange={this.handleChangeInputTool} />) },
      { size: 12, field: (<TextField label='Color' name='color' value={color} margin='none' required={true} onChange={this.handleChangeInputColor} />) }
    ]

    return(
      <React.Fragment>
        <Heading title='Update Tool' />

        <Form onSubmit={this.handleUpdateTool} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default ToolUpdate