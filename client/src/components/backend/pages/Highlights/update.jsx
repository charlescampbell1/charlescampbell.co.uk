import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'
import { TextField, KeyboardDatePicker } from 'mui-rff'
import DateFnsUtils from '@date-io/date-fns';

const validate = values => {
  const errors = {};

  if(!values.title) { errors.title = 'Required!' }
}

class HighlightUpdate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      title: '',
      company: '',
      date_from: '',
      date_to: '',
      description: ''
    }
  }

  componentDidMount = async () => {
    const { id } = this.state
    const highlight = await api.getHighlightById(id)

    this.setState({
      title: highlight.data.data.title,
      company: highlight.data.data.company,
      date_from: highlight.data.data.date_from,
      date_to: highlight.data.data.date_to,
      description: highlight.data.data.description
    })
  }

  handleChangeInputTitle = async event => {
    const title = event.target.value
    this.setState({ title })
  }

  handleChangeInputCompany = async event => {
    const company = event.target.value
    this.setState({ company })
  }

  handleChangeInputDateFrom = async event => {
    const date_from = event.target.value
    this.setState({ date_from })
  }

  handleChangeInputDateTo = async event => {
    const date_to = event.target.value
    this.setState({ date_to })
  }

  handleChangeInputDescription = async event => {
    const description = event.target.value
    this.setState({ description })
  }

  handleUpdateHighlight = async () => {
    const { id, title, company, date_from, date_to, description } = this.state
    const payload = { title, company, date_from, date_to, description }

    await api.updateHighlightById(id, payload).then(res => {
      window.alert(`Highlight updated.`)
      this.props.history.push('/backend')
    })
  }

  render () {
    const { title, company, date_from, date_to, description } = this.state

    const formFields = [
      { size: 12, field: (<TextField label='Title' value={title} name='title' margin='none' required={true} onChange={this.handleChangeInputTitle} />) },
      { size: 12, field: (<TextField label='Company' value={company} name='company' margin='none' required={true} onChange={this.handleChangeInputCompany} />) },
      { size: 6, field: (<TextField name='date_from' value={date_from} views={["year", "month"]} label='Start date *' dateFunsUtils={DateFnsUtils} clearable onChange={this.handleChangeInputDateFrom} />)},
      { size: 6, field: (<TextField name='date_to' value={date_to} views={["year", "month"]} label='End date' dateFunsUtils={DateFnsUtils} clearable placeholder='Leave blank for present' onChange={this.handleChangeInputDateTo} />)},
      { size: 12, field: (<TextField label='Description' value={description} name='description' margin='none' required={true} onChange={this.handleChangeInputDescription} />) }
    ]

    return(
      <React.Fragment>
        <Heading title='Update Highlight'/>

        <Form onSubmit={this.handleUpdateHighlight} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button type='button' variant='contained' onClick={form.reset} disabled={submitting || pristine}>Reset</Button>
                </Grid>

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default HighlightUpdate
