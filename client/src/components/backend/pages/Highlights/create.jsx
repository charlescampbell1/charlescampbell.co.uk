import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'
import { TextField, KeyboardDatePicker } from 'mui-rff'
import DateFnsUtils from '@date-io/date-fns';

const formFields = [
  { size: 12, field: (<TextField label='Title' name='title' margin='none' required={true} />) },
  { size: 12, field: (<TextField label='Company' name='company' margin='none' required={true} />) },
  { size: 6, field: (<KeyboardDatePicker name='date_from' views={["year", "month"]} label='Start date *' dateFunsUtils={DateFnsUtils} clearable />)},
  { size: 6, field: (<KeyboardDatePicker name='date_to' views={["year", "month"]} label='End date' dateFunsUtils={DateFnsUtils} clearable placeholder='Leave blank for present' />)},
  { size: 12, field: (<TextField label='Description' name='description' margin='none' required={true} />) }
]

const validate = values => {
  const errors = {};

  if(!values.title) { errors.title = 'Required!' }
}

const onSubmit = async values => {
  await api.insertHighlight(values).then(res => {
    window.alert('Highlight created.')
  })
}

class HighlightCreate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      title: '',
      company: '',
      date_from: '',
      date_to: '',
      description: ''
    }
  }

  render () {
    return(
      <React.Fragment>
        <Heading title='Create Highlight'/>

        <Form onSubmit={onSubmit} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button type='button' variant='contained' onClick={form.reset} disabled={submitting || pristine}>Reset</Button>
                </Grid>

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default HighlightCreate
