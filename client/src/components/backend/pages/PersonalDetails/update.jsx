import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { TextField } from 'mui-rff'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'

const validate = values => {
  const errors = {};

  if(!values.name) { errors.name = 'Required!' }
}

class PersonalDetailUpdate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      name: '',
      position: ''
    }
  }

  componentDidMount = async () => {
    const { id } = this.state
    const personalDetail = await api.getPersonalDetailById(id)

    this.setState({
      name: personalDetail.data.data.name,
      position: personalDetail.data.data.position,
    })
  }

  handleChangeInputName = async event => {
    const name = event.target.value
    this.setState({ name })
  }

  handleChangeInputPosition = async event => {
    const position = event.target.value
    this.setState({ position })
  }

  handleUpdatePersonalDetail = async () => {
    const { id, name, position } = this.state
    const payload = { name, position }

    await api.updatePersonalDetailById(id, payload).then(res => {
      window.alert(`Personal Detail Updated.`)
      this.setState({
        name: '',
        position: ''
      })
      this.props.history.push('/backend')
      window.location.reload()
    })
  }

  render () {
    const { name, position } = this.state

    const formFields = [
      { size: 12, field: (<TextField label='Full Name' name='name' value={name} margin='none' required={true} onChange={this.handleChangeInputName} />) },
      { size: 12, field: (<TextField label='Position' name='position' value={position} margin='none' required={true} onChange={this.handleChangeInputPosition} />) }
    ]


    return(
      <React.Fragment>
        <Heading title='Update Intro'/>

        <Form onSubmit={this.handleUpdatePersonalDetail} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default PersonalDetailUpdate