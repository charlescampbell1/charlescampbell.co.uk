import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { TextField } from 'mui-rff'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'

const formFields = [
  { size: 12, field: (<TextField label='Name' name='name' margin='none' required={true} />) },
  { size: 12, field: (<TextField label='URL' name='url' margin='none' required={true} />) }
]

const validate = values => {
  const errors = {};

  if(!values.name) { errors.name = 'Required!' }
}

const onSubmit = async values => {
  await api.insertProject(values).then(res => {
    window.alert('Project created.')
  })
}

class ProjectCreate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      url: ''
    }
  }

  render () {
    return(
      <React.Fragment>
        <Heading title='Create Project'/>

        <Form onSubmit={onSubmit} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button type='button' variant='contained' onClick={form.reset} disabled={submitting || pristine}>Reset</Button>
                </Grid>

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default ProjectCreate