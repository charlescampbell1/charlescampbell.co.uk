import React, { Component } from 'react'
import { Heading } from '../../../frontend/atoms'
import { Form } from 'react-final-form'
import { TextField } from 'mui-rff'
import { Paper, Grid, Button } from '@material-ui/core'
import api from '../../../../api'

const validate = values => {
  const errors = {};

  if(!values.name) { errors.name = 'Required!' }
  if(!values.url) { errors.url = 'Required!' }
}

class ProjectUpdate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      name: '',
      url: ''
    }
  }

  componentDidMount = async () => {
    const { id } = this.state
    const name = await api.getProjectById(id)

    this.setState({
      name: name.data.data.name,
      url: name.data.data.url
    })
  }

  handleChangeInputProject = async event => {
    const name = event.target.value
    this.setState({ name })
  }

  handleChangeInputColor = async event => {
    const url = event.target.value
    this.setState({ url })
  }

  handleUpdateProject = async () => {
    const { id, name, url } = this.state
    const payload = { name, url }

    await api.updateProjectById(id, payload).then(res => {
      window.alert(`Project updated.`)
      this.setState({
        name: '',
        url: ''
      })
      this.props.history.push('/backend')
      window.location.reload()
    })
  }

  render() {
    const { name, url } = this.state

    const formFields = [
      { size: 12, field: (<TextField label='Project' name='name' value={name} margin='none' required={true} onChange={this.handleChangeInputProject} />) },
      { size: 12, field: (<TextField label='Color' name='url' value={url} margin='none' required={true} onChange={this.handleChangeInputColor} />) }
    ]

    return(
      <React.Fragment>
        <Heading title='Update Project' />

        <Form onSubmit={this.handleUpdateProject} validate={validate} render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems='flex-start' spacing={2}>
                {formFields.map((item, idx) => (
                  <Grid item xs={item.size} key={idx}>
                    {item.field}
                  </Grid>
                ))}

                <Grid item style={{ marginTop: 16}}>
                  <Button className='submit-button' variant='contained' type='submit' disabled={submitting}>Submit</Button>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
        />
      </React.Fragment>
    )
  }
}

export default ProjectUpdate