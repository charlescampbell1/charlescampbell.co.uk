import axios from 'axios'

const api = axios.create({ baseURL: 'http://localhost:3000/api' })

export const insertPersonalDetail = payload => api.post(`/personal_detail`, payload)
export const getPersonalDetail = () => api.get(`/personal_detail`)
export const getAllPersonalDetails = () => api.get(`/personal_details`)
export const updatePersonalDetailById = (id, payload) => api.put(`/personal_detail/${id}`, payload)
export const deletePersonalDetailById = id => api.delete(`/personal_detail/${id}`)
export const getPersonalDetailById = id => api.get(`/personal_detail/${id}`)

export const insertProfileLink = payload => api.post(`/profile_link`, payload)
export const getAllProfileLinks = () => api.get(`/profile_links`)
export const updateProfileLinkById = (id, payload) => api.put(`/profile_link/${id}`, payload)
export const deleteProfileLinkById = id => api.delete(`/profile_link/${id}`)
export const getProfileLinkById = id => api.get(`/profile_link/${id}`)

export const insertTool = payload => api.post(`/tool`, payload)
export const getAllTools = () => api.get(`/tools`)
export const updateToolById = (id, payload) => api.put(`/tool/${id}`, payload)
export const deleteToolById = id => api.delete(`/tool/${id}`)
export const getToolById = id => api.get(`/tool/${id}`)

export const insertProject = payload => api.post(`/project`, payload)
export const getAllProjects = () => api.get(`/projects`)
export const updateProjectById = (id, payload) => api.put(`/project/${id}`, payload)
export const deleteProjectById = id => api.delete(`/project/${id}`)
export const getProjectById = id => api.get(`/project/${id}`)

export const insertHighlight = payload => api.post(`/highlight`, payload)
export const getAllHighlights = () => api.get(`/highlights`)
export const updateHighlightById = (id, payload) => api.put(`/highlight/${id}`, payload)
export const deleteHighlightById = id => api.delete(`/highlight/${id}`)
export const getHighlightById = id => api.get(`/highlight/${id}`)

const apis = {
    insertPersonalDetail, getAllPersonalDetails, updatePersonalDetailById, deletePersonalDetailById, getPersonalDetailById, getPersonalDetail,
    insertProfileLink, getAllProfileLinks, updateProfileLinkById, deleteProfileLinkById, getProfileLinkById,
    insertTool, getAllTools, updateToolById, deleteToolById, getToolById,
    insertProject, getAllProjects, updateProjectById, deleteProjectById, getProjectById,
    insertHighlight, getAllHighlights, updateHighlightById, deleteHighlightById, getHighlightById,
}

export default apis