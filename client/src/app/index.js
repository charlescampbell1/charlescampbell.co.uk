import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Intro, Footer } from '../components/frontend/organisms'
import { Home, Education, Projects } from '../components/frontend/pages'
import { BackendHome, NewPersonalDetail, UpdatePersonalDetail, CreateTool, UpdateTool, CreateProject, UpdateProject, CreateHighlight, UpdateHighlight } from '../components/backend/pages'

import '../components/styles/main.scss'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <Router>
      <div className='container'>
        <Intro />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/education' exact component={Education} />
          <Route path='/projects' exact component={Projects} />
          <Route path='/backend' exact component={BackendHome} />

          <Route path='/personal_detail/new' exact component={NewPersonalDetail} />
          <Route path='/personal_detail/update/:id' exact component={UpdatePersonalDetail} />

          <Route path='/tool/new' exact component={CreateTool} />
          <Route path='/tool/update/:id' exact component={UpdateTool} />

          <Route path='/project/new' exact component={CreateProject} />
          <Route path='/project/update/:id' exact component={UpdateProject} />

          <Route path='/highlight/new' exact component={CreateHighlight} />
          <Route path='/highlight/update/:id' exact component={UpdateHighlight} />
        </Switch>
      </div>
      <Footer />
    </Router>
  )
}

export default App